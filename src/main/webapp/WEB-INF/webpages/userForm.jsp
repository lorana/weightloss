<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<html>
<head>
</head>
<body>
	<h3>Welcome, Enter The User Details</h3>
	<form:form method="POST" action="#" modelAttribute="user">
		<table>
			<tr>
				<td><form:label path="id">Id: </form:label></td>
				<td><form:input path="id" /></td>
			</tr>
			<tr>
				<td><form:label path="username">
                      User Name: </form:label></td>
				<td><form:input path="username" /></td>
			</tr>
			<tr>
				<td><form:label path="firstName">First Name: </form:label></td>
				<td><form:input path="firstName" /></td>
			</tr>
			<tr>
				<td><form:label path="lastName">
                      Last Name: </form:label></td>
				<td><form:input path="lastName" /></td>
			</tr>
			<tr>
				<td><form:label path="email">
                      Email: </form:label></td>
				<td><form:input path="email" /></td>
			</tr>
			<tr>
				<td><form:label path="phoneNumber">
                      Phone Number: </form:label></td>
				<td><form:input path="phoneNumber" /></td>
			</tr>
			<tr>
				<td><form:label path="password">
                      Password: </form:label></td>
				<td><form:input path="password" /></td>
			</tr>
			<tr>
				<td><form:label path="birthDate">
                      Birth Date: </form:label></td>
				<td><form:input path="birthDate" /></td>
			</tr>
			<tr>
				<td><input type="submit" value="Submit" /></td>
			</tr>
			<tr>
				<tdpath="gender">
                     Gender: 
                     <br>
                     Male:
                    <input type="radio" name="gender"
							value="MALE" path="user.gender" items="${user.gender}">
					Female:
							<input type="radio" name="gender"
							value="MALE" path="user.gender" items="${user.gender}">
					</td>
				<td><form:input path="gender" /></td>
			</tr>
		</table>
		</form:form>
</body>
</html>