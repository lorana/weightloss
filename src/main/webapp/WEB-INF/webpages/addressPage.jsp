<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>User's addresses</title>
</head>
<body>
	<table style="border: 1px solid;">
		<c:forEach var="addresses" items="${addresses}">
			<tr>
				<td>${addresses.id}</td>
				<td>${addresses.city}</td>
				<td>${addresses.street}</td>
				<td>${addresses.country}</td>
			</tr>
		</c:forEach>
	</table>


</body>
</html>