package org.sda.weightlossassistant.dao;

import java.util.List;

import org.sda.weightlossassistant.entity.User;

public interface UserDao {

	public void saveUser(User user);

	public List<User> getAllUsers();

}
