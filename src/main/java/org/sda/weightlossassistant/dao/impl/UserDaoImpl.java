package org.sda.weightlossassistant.dao.impl;

import java.util.List;

import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.sda.weightlossassistant.dao.UserDao;
import org.sda.weightlossassistant.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
@Repository
public class UserDaoImpl implements UserDao {
	@Autowired
	private SessionFactory sessionFactory;

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Override
	public void saveUser(User user) {
		sessionFactory.getCurrentSession().save(user);

	}

	@Override
	public List<User> getAllUsers() {
		Query query = sessionFactory.getCurrentSession().createQuery("from User");

		return query.list();

	}

}
