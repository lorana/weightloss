package org.sda.weightlossassistant.controller;


import org.sda.weightlossassistant.entity.User;
import org.sda.weightlossassistant.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class UserController {

	@Autowired
	private UserService userService;

//	@RequestMapping(value = "/user", method = RequestMethod.GET)
//	public String getUser(Model model) {
//
//		User user = new User(1, "Lorena", "Ana", "123456789");
//		User user2 = new User(2, "Floarea", "Floricica", "123456789");
//		model.addAttribute("user", user);
//		model.addAttribute("user2", user2);
//
//		return "user";
//
//	}
//
//	@RequestMapping(value = "/user/{id}", method = RequestMethod.GET)
//	public String getUserById(@PathVariable(name = "id") int id, Model model) {
//		System.out.println("The id we received is: " + id);
//		User user1 = new User();
//		user1.setId(id);
//		user1.setFirstName("Ion");
//		user1.setLastName("Pop");
//		user1.setPhoneNumber("1254898464");
//		model.addAttribute("user", user1);
//
//		return "user";
//
//	}

//	@RequestMapping(value = "/user/{id}/addresses", method = RequestMethod.GET)
//	public String getAdressesForUserId(Model model, @PathVariable(value = "id") int id) {
//
//		User user = userService.getUserById(id);
//		model.addAttribute("addresses", user.getAdresses());
//
//		return "addressPage";
//
//	}

//	@RequestMapping(value = "users/{userId}/address/{adressId}", method = RequestMethod.GET)
//	public String getAdressesbyID(Model model, @PathVariable(value = "userId") int userId,
//			@PathVariable(value = "adressId") int adressId) {
//		User user = userService.getUserById(1);
//		Address address = userService.getAdressByIdFromList(adressId, user.getAdresses());
//		model.addAttribute("address", address);
//
//		return "adresses";
//
//	}

//	@RequestMapping(value = "/userWithQueryParam", method = RequestMethod.GET)
//	public String getUserWithQueryParams(Model model, @RequestParam(value = "firstName") String firstName) {
//
//		if (firstName.equals("Lorena")) {
//
//			User user = new User(1, "Lorena", "Ana", "123456789");
//			model.addAttribute("user", user);
//		} else {
//			User user2 = new User(2, "Floarea", "Floricica", "168177999511456");
//			model.addAttribute("user2", user2);
//		}
//
//		return "user";
//	}

	@RequestMapping(value = "/addUser", method = RequestMethod.GET)
	public String getUserForm(Model model) {

		User user = new User();
		model.addAttribute("user", user);
		return "userForm";

	}
	
	@RequestMapping(value = "/addUser", method = RequestMethod.POST)
	public String saveUser(Model model, @ModelAttribute("user") User user) {
		model.addAttribute("user1", user);
		
		return "user";
	}

}
