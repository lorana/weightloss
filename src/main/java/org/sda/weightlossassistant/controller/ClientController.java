package org.sda.weightlossassistant.controller;

import org.sda.weightlossassistant.entity.Client;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class ClientController {
		
		@RequestMapping(value="/clients", method=RequestMethod.GET)
		public String getUser(Model model) {
			
			Client client=new Client(1, "Ion", "Pop", "ionpop@gmail.com", "0755452635");
			model.addAttribute("client", client);
			
			return "clients";

		}
}
