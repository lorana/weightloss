package org.sda.weightlossassistant.service.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.sda.weightlossassistant.dao.UserDao;
import org.sda.weightlossassistant.entity.User;
import org.sda.weightlossassistant.service.UserService;
import org.springframework.stereotype.Service;
@Service
public class UserServiceImpl implements UserService {

	private UserDao userDao;

	public UserDao getUserDao() {
		return userDao;
	}

	public void setUserDao(UserDao userDao) {
		this.userDao = userDao;
	}

	@Override
	public void saveUser(User user) {
		userDao.saveUser(user);

	}

	@Transactional
	public List<User> getAllUsers() {
		
		return userDao.getAllUsers();
	}

}
