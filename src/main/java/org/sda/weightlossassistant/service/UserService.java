package org.sda.weightlossassistant.service;

import java.util.List;

import org.sda.weightlossassistant.entity.User;


public interface UserService {
	public void saveUser(User user);

	public List<User> getAllUsers();


}
