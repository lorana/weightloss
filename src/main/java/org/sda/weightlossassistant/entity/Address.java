package org.sda.weightlossassistant.entity;

public class Address {
	private int id;
	private String city;
	private String country;
	private String street;

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountry() {
		return country;
	}

	@Override
	public String toString() {
		return "Address [id=" + id + ", city=" + city + ", country=" + country + ", street=" + street + "]";
	}

	public Address(int id, String city, String country, String street) {
		super();
		this.id = id;
		this.city = city;
		this.country = country;
		this.street = street;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

}
